const http = require('http');
const fs = require('fs');
const url = require('url');

module.exports = () => {
    if (!fs.existsSync('./file')) {
        fs.mkdirSync('./file');
    }
    http.createServer((req, res) => {
        const { pathname, query } = url.parse(req.url, true);
        const reqTime = Date.now();
        if (req.method === 'POST') {
            if (!query.filename) {
                res.writeHead(400, { 'Content-type': 'text/html' });
                res.end(`No filename specified`);
                return;
            }
            fs.writeFile(`./file/${query.filename}`, query.content, { flag: 'w' }, (err) => {
                if (err) {
                    console.log(err);
                    res.writeHead(400, { 'Content-type': 'text/html' });
                    res.end(`There was an error creating file ${query.filename}`);
                    return;
                }
                res.writeHead(200);
                res.end();
                updateLogs(`New file with name '${query.filename}' saved`, reqTime);
                return;
            });
        } else if (pathname === "/logs") {
            fs.readFile(`./logs.txt`, 'utf8', (err, data) => {
                if (err) {
                    res.writeHead(400, { 'Content-type': 'text/html' });
                    res.end(`Error reading logs file`);
                    return;
                }
                let logs = [];
                try {
                    logs = JSON.parse(data).logs;
                } catch (e) {
                }
                if (query.from) {
                    let timeFrom = parseInt(query.from) || 0;
                    logs = logs.filter((log) => log.time >= timeFrom);
                }
                if (query.to) {
                    let timeTo = parseInt(query.to) || 0;
                    logs = logs.filter((log) => timeTo >= log.time);
                }
                res.writeHead(200, { 'Content-type': 'application/json' });
                res.end(JSON.stringify({ 'logs': logs }, null, '\t'));
                updateLogs(`Logs were read`, reqTime);
            });
        }
        else {
            fs.readFile(`.${pathname}`, 'utf8', (err, content) => {
                if (err) {
                    res.writeHead(400, { 'Content-type': 'text/html' });
                    res.end(`No files with name "${pathname}" found`);
                    return;
                }
                res.writeHead(200, { 'Content-type': 'text/html' });
                res.end(content);
                updateLogs(`File with name '${pathname}' was read`, reqTime);
            });
        }
    }).listen(process.env.PORT || 8080);

    function updateLogs(message, time) {
        fs.readFile('./logs.txt', 'utf8', (err, data) => {
            if (err) {
                console.log(err);
                return;
            }
            let logs = [];
            try {
                logs = JSON.parse(data).logs;
            } catch (e) {
            }
            logs.push({ 'message': message, time: time });
            fs.writeFile('./logs.txt', JSON.stringify({ logs }, null, '\t'), 'utf8', (err) => {
                if (err) {
                    console.log(err);
                    return;
                }

            });
        });
    }
}

